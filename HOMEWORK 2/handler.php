<?php
	//include 'users.php';
	session_start();
	if ( isset($_POST['login']) && isset($_POST['password'])) {
		
		if (file_exists( 'userdata/'.$_POST['login'].'.json' )) {
			
			$file = fopen('userdata/'.$_POST['login'].'.json', 'r');
			
			while (!feof($file)) {
				$buffer = fgets($file, 4096);
				//$bufferArray = json_decode($buffer);
				$bufferArray = json_decode($buffer);
			}
			
			fclose($file);
			
			if($_POST['password'] == $bufferArray->password) {
				$_SESSION["login"] 	   = $bufferArray->login;
				$_SESSION["firstName"] = $bufferArray->firstName;
				$_SESSION["lastName"]  = $bufferArray->lastName;
				$_SESSION["role"]      = $bufferArray->role;
		
				header('Location: authentification.php');
				
			}
			
		}
		
	} 
	else {
		header('Location: authentification.php');
	}
?>
<html>
	<head> 
		<title>Homework #2</title>
		<meta charset="UTF-8">
		<link rel="stylesheet" type="text/css" href="style.css" >
	</head>
	<body>
		<?php	
			echo '<p>Некорректные логин или пароль</p>';
			echo "<p><a href='authentification.php'>Вернуться на страницу авторизации</a></p>";
			echo "<p><a href='index.php'>Вернуться на главную страницу</a></p>";
		?>
	</body>
</html>

