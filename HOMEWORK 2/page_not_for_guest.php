<?php 
	session_start();
?>
<html>
	<head>
		<title>Homework #2</title>
		<meta charset="UTF-8">
		<link rel="stylesheet" type="text/css" href="style.css" >
	</head>
	<body>
		<?php 
			if ( isset ($_SESSION["login"]) == false) {
				echo "<p>Вы не авторизовались на сайте. Конфиденциальная информация Вам недоступна. </p>";
				echo "<p><a href='index.php'>Вернуться на главную страницу</a></p>";
			}
			else {
				echo "<h1>Страница с текстом, доступным только авторизованному пользователю</h1>";
				echo "<p>Пароль для доступа в базу данных: n24RgtRj634jf1</p>";
				echo "<p><a href='index.php'>Вернуться на главную страницу</a></p>";
			}
		?>
	</body>
</html>

