<?php
	session_start();
?>
<html>
	<head> 
		<title>Homework #2</title>
		<meta charset="UTF-8">
		<link rel="stylesheet" type="text/css" href="style.css" >
	</head>
	<body>
		<h1>Страница авторизации</h1>
		<?php if ( isset($_SESSION['login']) ) {
			echo "<p> {$_SESSION['login']}, Вы авторизованы!</p>";
			echo "<p><a href='index.php'>Вернуться на главную страницу</a></p>";
			echo "<p><a href='destroy-session.php'>Выйти и вернуться на главную страницу</a></p>";
		}
		else {
			echo "<p>Вы не авторизованы!</p>";
			echo "<p>Введите логин и пароль:</p>";
			
			echo "<form method='POST' action='handler.php'>";
			echo "<label>Логин:</label>";
			echo "<input type='text' name='login'>";
			echo "<br><br>";
			echo "<label>Пароль:</label>";
			echo "<input type='password' name='password'>";
			echo "<br><br>";
			echo "<button type='submit'>Отправить</button>";
			echo "</form>";
			
			echo "<p><a href='index.php'>Вернуться на главную страницу</a></p>";
			
		}
		?>
	</body>
</html>
