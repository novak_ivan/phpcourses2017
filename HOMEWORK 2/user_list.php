<?php 
	session_start();
?>
<html>
	<head>
		<title>Homework #2</title>
		<meta charset="UTF-8">
		<link rel="stylesheet" type="text/css" href="style.css" >
	</head>
	<body>
		<?php 
			echo "<h1>Список пользователей</h1>";
			if ( isset ($_SESSION["login"]) == false) {
				echo "<p>Вы не авторизовались на сайте. </p>";
				echo "<p><a href='index.php'>Вернуться на главную страницу</a></p>";
			}
			else {
				$path = scandir("userdata");
				
				foreach($path as $k){
					$findJSON = strpos($k,".json");
					if ($findJSON !== false) {
						$login = substr($k, 0,$findJSON);
						echo "<p><a href='info_user_for_admin.php?user={$login}'>".$login."</a></p>";
					}
				}
				echo "<p><a href='index.php'>Вернуться на главную страницу</a></p>";
			}
		?>
	</body>
</html>

