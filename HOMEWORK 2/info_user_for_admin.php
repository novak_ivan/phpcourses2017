<?php 
	session_start();
?>
<html>
	<head>
		<title>Homework #2</title>
		<meta charset="UTF-8">
		<link rel="stylesheet" type="text/css" href="style.css" >
	</head>
	<body>
		<?php 
			if ( isset ($_SESSION["login"]) == false) {
				echo "<p>Вы не авторизовались на сайте. </p>";
				echo "<p><a href='index.php'>Вернуться на главную страницу</a></p>";
			}
			else {
				
				echo "<p>Информация администратору о пользователе: ".$_GET["user"]. "</p>";
				
				$file = fopen('userdata/'.$_GET["user"].'.json', 'r');
				
				while (!feof($file)) {
					$buffer = fgets($file, 4096);
					//$bufferArray = json_decode($buffer);
					$bufferArray = json_decode($buffer);
				}
				
				fclose($file);
				
				echo "<p>Логин: ".$bufferArray->login."</p>";
				echo "<p>Имя: ".$bufferArray->firstName."</p>";
				echo "<p>Фамилия: ".$bufferArray->lastName."</p>";
				echo "<p>Пароль: ".$bufferArray->password."</p>";
				echo "<p>Роль (0-админ, 1-модератор, 2-зарегистрированный пользователь): ".$bufferArray->role."</p>";
				
				
				echo "<p><a href='index.php'>Вернуться на главную страницу</a></p>";
			}
		?>
	</body>
</html>


