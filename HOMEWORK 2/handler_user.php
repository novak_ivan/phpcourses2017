<?php 
	session_start();
?>
<html>
	<head>
		<title>Homework #2</title>
		<meta charset="UTF-8">
		<link rel="stylesheet" type="text/css" href="style.css" >
	</head>
	<body>
		<?php
			session_start();
			if ( isset ($_SESSION["login"]) == false) {
				echo "<p>Вы не авторизовались на сайте. </p>";
				echo "<p><a href='index.php'>Вернуться на главную страницу</a></p>";
			}
			else {
				if (file_exists( 'userdata/'.$_SESSION['login'].'.json' )) {
					
					$file = fopen('userdata/'.$_SESSION['login'].'.json', 'r');
					
					while (!feof($file)) {
						$buffer = fgets($file, 4096);
						//$bufferArray = json_decode($buffer);
						$bufferArray = json_decode($buffer);
					}
					
					fclose($file);
					
					
					$arrayNewUser = array(
					'login'        => $_SESSION["login"],
					'firstName'    => $_POST["firstName"],
					'lastName'     => $_POST["lastName"],
					'password'     => $_POST["password"],
					'role'         => $bufferArray->role,
					);
					
					$_SESSION["firstName"] = $_POST["firstName"];
					$_SESSION["lastName"]  =  $_POST["lastName"];
					
					$encodeArrayNewUserJSON = json_encode($arrayNewUser);
					
					$newUserFile =  "userdata/";
					$newUserFile .=  strtolower($_SESSION["login"]).".json";
					
					file_put_contents($newUserFile, $encodeArrayNewUserJSON);
					echo "<p>Информация успешно изменена. </p>";
					echo "<p><a href='index.php'>Вернуться на главную страницу</a></p>";
				}
				
				else echo "<p><a href='index.php'>Вернуться на главную страницу</a></p>";
				
			}
			
		?>
		
	</body>
</html>
