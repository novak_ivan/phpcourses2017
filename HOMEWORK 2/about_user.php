<?php 
	session_start();
?>
<html>
	<head>
		<title>Homework #2</title>
		<meta charset="UTF-8">
		<link rel="stylesheet" type="text/css" href="style.css" >
	</head>
	<body>
		<?php 
			if ( isset ($_SESSION["login"]) == false) {
				echo "<p>Вы не авторизовались на сайте. </p>";
				echo "<p><a href='index.php'>Вернуться на главную страницу</a></p>";
			}
			else {
				echo "<h1>Информация о текущем пользователе</h1>";
				echo "<p>Логин: ". $_SESSION["login"]."</p>";
				echo "<p>Имя: ". $_SESSION["firstName"]."</p>";
				echo "<p>Фамилия: ". $_SESSION["lastName"]."</p>";
				echo "<p><a href='index.php'>Вернуться на главную страницу</a></p>";
				echo "<p><a href='destroy-session.php'>Выйти и вернуться на главную страницу</a></p>";
			}
		?>
	</body>
</html>

