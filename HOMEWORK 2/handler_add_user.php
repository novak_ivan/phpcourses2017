<?php
	session_start();
?>	
<html>
	<head>
		<title>Homework #2</title>
		<meta charset="UTF-8">
		<link rel="stylesheet" type="text/css" href="style.css" >
	</head>
	<body>
		<?php 
			if ( isset ($_SESSION["login"]) == false) {
				echo "<p>Вы не авторизовались на сайте. </p>";
				echo "<p><a href='index.php'>Вернуться на главную страницу</a></p>";
			}
			else {
				if ( isset($_POST["login"]) && isset($_POST["firstName"]) && isset($_POST["lastName"]) && 
				isset($_POST["password"])&& isset($_POST["role"])) {
					if ( $_POST["login"] !== "" && $_POST["role"] !== "" && $_POST["password"] !== "") { 
						$arrayNewUser = array(
						'login'        => $_POST["login"],
						'firstName'    => $_POST["firstName"],
						'lastName'     => $_POST["lastName"],
						'password'     => $_POST["password"],
						'role'         => $_POST["role"],
						);
						$encodeArrayNewUserJSON = json_encode($arrayNewUser);
						
						$newUserFile =  "userdata/";
						$newUserFile .=  strtolower($_POST["login"]).".json";
						
						file_put_contents($newUserFile, $encodeArrayNewUserJSON);
						
						echo "<p>Пользователь успешно добавлен.</p>";
						echo "<p><a href='index.php'>Вернуться на главную страницу</a></p>";
					}
					else {
						echo "<p>Пользователь не добавлен. Некорректно заполнены поля.</p>";
						echo "<p><a href='index.php'>Вернуться на главную страницу</a></p>";
					}
				}
				else {
					echo '<p>Пользователь не добавлен.</p>';
					echo "<p><a href='index.php'>Вернуться на главную страницу</a></p>";
				}
			}
			
		?>		
		
	</body>
</html>