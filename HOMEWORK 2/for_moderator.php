<?php 
	session_start();
?>
<html>
	<head>
		<title>Homework #2</title>
		<meta charset="UTF-8">
		<link rel="stylesheet" type="text/css" href="style.css" >
	</head>
	<body>
		<?php 
			if ( isset ($_SESSION["login"]) == false) {
				echo "<p>Вы не авторизовались на сайте. </p>";
				echo "<p><a href='index.php'>Вернуться на главную страницу</a></p>";
			}
			else {
				$file = fopen('userdata/'.$_SESSION["login"].'.json', 'r');
				while (!feof($file)) {
					$buffer = fgets($file, 4096);
					//$bufferArray = json_decode($buffer);
					$bufferArray = json_decode($buffer);
				}
				fclose($file);
				$role = $bufferArray->role;
				if ($role == 1) {
					echo "<p>Вы прошли проверку,  у Вас права модератора.</p>";
					echo "<p>Пароль для доступа в секретную базу МОДЕРАТОРОВ: Путин Владимир Владимирович</p>";
					echo "<p><a href='index.php'>Вернуться на главную страницу</a></p>";
				}
				else {
					echo "<p>Вам нет доступа для просмотра текущего контента. Вы не модератор.</p>";
					echo "<p><a href='index.php'>Вернуться на главную страницу</a></p>";
				}
			}
		?>
	</body>
</html>

