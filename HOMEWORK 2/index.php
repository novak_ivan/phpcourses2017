<?php 
	session_start();
?>
<html>
	<head>
		<title>Homework #2</title>
		<meta charset="UTF-8">
		<link rel="stylesheet" type="text/css" href="style.css" >
	</head>
	<body>
		<h1>Главная страница домашнего задания 2</h1>
		<ul>
			<li><a href="authentification.php">Страница авторизации</a></li>
			<li><a href="about_user.php">Информация о текущем пользователе</a></li>
			<li><a href="page_not_for_guest.php">Страница только для авторизованных пользователей</a></li>
			<li><a href="change_user.php">Отредактировать информацию о текущем пользователе</a></li>
			<?php 
				if ( isset($_SESSION['role']) ) {
					if ($_SESSION['role'] == "1") echo "<li><a href='for_moderator.php'>Страница только для модераторов (роль №1)</a></li>";
					if ($_SESSION['role'] == "0") {
						echo "<li><a href='add_user.php'>Добавить пользователя</a></li>";
						echo "<li><a href='user_list.php'>Список пользователей</a></li>";
					}
				}
				if ( isset($_SESSION['login']) ) echo "<p><a href='destroy-session.php'>Выйти</a></p>";
			?>
		</ul>
	</body>
</html>

