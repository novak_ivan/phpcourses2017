<?php 
	session_start();
?>
<html>
	<head>
		<title>Homework #2</title>
		<meta charset="UTF-8">
		<link rel="stylesheet" type="text/css" href="style.css" >
	</head>
	<body>
		<?php 
			if ( isset ($_SESSION["login"]) == false) {
				echo "<p>Вы не авторизовались на сайте. </p>";
				echo "<p><a href='index.php'>Вернуться на главную страницу</a></p>";
			}
			else {
				echo "<h1>Добавление нового пользователя.</h1>";
				echo "<h3>Введите логин, фамилию, имя и пароль. </h2>";
				echo "<form method='POST' action='handler_add_user.php'>";
				echo "<label>Логин: </label>";
				echo "<input type='text' name='login'  >";
				echo "<br><br>";
				echo "<label>Имя: </label>";
				echo "<input type='text' name='firstName' >";
				echo "<br><br>";
				echo "<label>Фамилия: </label>";
				echo "<input type='text' name='lastName'  >";
				echo "<br><br>";
				echo "<label>Пароль: </label>";
				echo "<input type='password' name='password' >";
				echo "<br><br>";
				echo "<label>Роль пользователя(0-админ, 1-модератор, 2-зарегистрированный пользователь): </label>";
				echo "<input type='text' name='role' value='0'>";
				echo "<br><br>";
				echo "<button type='submit'>Отправить</button>";
				echo "</form>";
				echo "<p><a href='index.php'>Вернуться на главную страницу</a></p>";
			}
		?>
	</body>
</html>

