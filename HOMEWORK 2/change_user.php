<?php 
	session_start();
?>
<html>
	<head>
		<title>Homework #2</title>
		<meta charset="UTF-8">
		<link rel="stylesheet" type="text/css" href="style.css" >
	</head>
	<body>
		<?php 
			if ( isset ($_SESSION["login"]) == false) {
				echo "<p>Вы не авторизовались на сайте. </p>";
				echo "<p><a href='index.php'>Вернуться на главную страницу</a></p>";
			}
			else {
				echo "<h1>".$_SESSION["login"].", Вы при необходимости можете изменить Имя, Фамилию и Пароль </h1>";
				echo "<form method='POST' action='handler_user.php'>";
				echo "<label>Имя:</label>";
				echo "<input type='text' name='firstName' value='".$_SESSION["firstName"]."'>";
				echo "<br><br>";
				echo "<label>Фамилия:</label>";
				echo "<input type='text' name='lastName' value='".$_SESSION["lastName"]."'>";
				echo "<br><br>";
				echo "<label>Пароль: </label>";
				echo "<input type='password' name='password'>";
				echo "<br><br>";
				echo "<button type='submit'>Отправить</button>";
				echo "</form>";
				echo "<p><a href='index.php'>Вернуться на главную страницу</a></p>";
				echo "<p><a href='destroy-session.php'>Выйти</a></p>";
			}
		?>
	</body>
</html>

